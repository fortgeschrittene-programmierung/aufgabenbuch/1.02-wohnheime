# 1.02 Studierendenwohnheim

**Voraussetzungen**: Keine

**Ziel**:
Erstellung einer kleinen Java-Anwendung, um das Ein- und Ausziehen von Studierenden in einem Wohnheim zu simulieren. Dabei soll das System sicherstellen, dass Studierende nur einziehen können, wenn ausreichend Platz in einer WG vorhanden ist.

**Dauer**:< 90 Minuten

**Lösung**:
Die Lösung zur Implementierung dieses Szenarios befindet sich im Quellcode der Klassen `Student`, `SharedFlat` und `Dormitory`.

## Aufgabenstellung
In einem Studierendenwohnheim können mehrere WGs (Wohngemeinschaften) existieren, in denen Studierende wohnen. Es muss die Möglichkeit bestehen, dass neue Studierende einziehen oder ausziehen. Es darf nur ein Studierender einziehen, wenn in der entsprechenden WG noch Platz ist.

### Programmieren Sie die folgenden Klassen:

#### 1. **Student**:
   - **Attribute**:
     - `String name`: Der Name des Studierenden.
     - `String studyProgram`: Das Studienfach des Studierenden.
   - **Konstruktor**:
     - Parameter: `name` und `studyProgram`.
   - **Methoden**:
     - Getter & Setter für die Attribute `name` und `studyProgram`.

#### 2. **SharedFlat** (WG):
   - **Attribute**:
     - `java.util.ArrayList<Student>`: Liste von Studierenden, die in der WG wohnen.
     - `int maxNumberOfStudents`: Maximale Anzahl der Studierenden, die in der WG wohnen dürfen.
   - **Konstruktoren**:
     - Default-Konstruktor: Erstellt eine WG für maximal **einen** Studierenden.
     - Alternativer Konstruktor: Erstellt eine WG mit einer anzugebenden maximalen Anzahl von Studierenden.
   - **Methoden**:
     - `getStudents()`: Liefert die Liste der in der WG wohnenden Studierenden.
     - `getNumberOfStudents()`: Gibt die Anzahl der tatsächlich in der WG wohnenden Studierenden zurück.
     - `addStudent(Student student)`: Fügt einen Studierrnden zur WG hinzu. Sollte die WG voll sein, gibt das System eine Meldung aus: "This student can not move in." (via `System.out.println`)r
     
#### 3. **Dormitory** (Wohnheim):
   - **Attribute**:
     - `java.util.ArrayList<SharedFlat>`: Liste der vorhandenen WGs.
   - **Konstruktoren**:
     - Default-Konstruktor: Erstellt ein Wohnheim mit einer WG für **einen** Studierenden.
     - Alternativer Konstruktor: Erstellt ein Wohnheim mit einer übergebenen Liste von WGs.
   - **Methoden**:
     - `getTotalStudents()`: Ermittelt und gibt die Anzahl aller im Wohnheim lebenden Studierenden zurück.
     - `getStudentNames()`: Gibt eine Liste mit den Namen aller im Wohnheim lebenden Studierenden zurück (`java.util.ArrayList<String>`).

## Szenario in der ausführbaren Klasse `App`

Implementieren Sie eine `main`-Methode in der Klasse `App`, mit folgendem Szenario:

1. Erstellen Sie zwei WGs:
   - WG #1: Für maximal **2** Studierende.
   - WG #2: Für maximal **3** Studierende.

2. Speichern Sie die WGs in einer Liste und fügen Sie diese dem Wohnheim hinzu indem Sie ein neues Wohnheim erstellen und diesem die Liste der WGs übergeben. 

3. Legen Sie **6 Studierende** an:
   - Lassen Sie **zwei Studierende** in WG #1 einziehen.
   - Lassen Sie **drei Studierende** in WG #2 einziehen.
   - Versuchen Sie, den **sechsten Studierenden** in WG #2 einziehen zu lassen. Vergewissern Sie sich, dass dies nicht funktioniert.

4. Geben Sie die Anzahl aller im Wohnheim lebenden Studierenden sowie deren Namen auf der Konsole aus.

### Beispielausgabe:
```plaintext
Abe moved in!
Bob moved in!
Joe moved in!
Kay moved in!
Jim moved in!
Tom can't move in.
Names of students in the dormitory: [Abe, Bob, Joe, Kay, Jim]
Number of students living in the dormitory: 5